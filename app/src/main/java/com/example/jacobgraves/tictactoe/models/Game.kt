package com.example.jacobgraves.tictactoe.models

data class Game(var gameBoard: Array<CharArray>, var winner: Char) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Game

        if (!gameBoard.contentDeepEquals(other.gameBoard)) return false
        if (winner != other.winner) return false

        return true
    }

    override fun hashCode(): Int {
        var result = gameBoard.contentDeepHashCode()
        result = 31 * result + winner.hashCode()
        return result
    }
}