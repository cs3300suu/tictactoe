package com.example.jacobgraves.tictactoe.views.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import com.example.jacobgraves.tictactoe.R
import com.example.jacobgraves.tictactoe.models.DisplaySnackbar

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    var gameBoard: Array<CharArray> = Array(3){ CharArray(3) }
    var turn = 'X'
    var tableLayout: TableLayout? = null
    var turnTextView: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            DisplaySnackbar(view, "Hello, Kotlin!")
        }

        turnTextView = findViewById(R.id.turnTextView)
        tableLayout = findViewById(R.id.table_layout)

        startNewGame(true)
    }

    private fun startNewGame(setClickListener: Boolean) {
        turn = 'X'
        turnTextView?.text = String.format(getString(R.string.turn), turn)
        for (i in 0 until gameBoard.size){
            for (j in 0 until gameBoard[i].size){
                gameBoard[i][j] = ' '
                val cell = (tableLayout?.getChildAt(i) as TableRow).getChildAt(j) as TextView
                cell.text = ""

                if(setClickListener) {
                    cell.setOnClickListener { cellClickListener(i, j) }
                }

            }
        }
    }

    private fun cellClickListener(row: Int, column: Int){
        gameBoard[row][column] = turn
        ((tableLayout?.getChildAt(row) as TableRow).getChildAt(column) as TextView).text = turn.toString()
        turn = if ('X' == turn) 'O' else 'X'
        turnTextView?.text = String.format(getString(R.string.turn), turn)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
