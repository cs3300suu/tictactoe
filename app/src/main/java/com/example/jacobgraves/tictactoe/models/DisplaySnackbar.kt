package com.example.jacobgraves.tictactoe.models

import android.support.design.widget.Snackbar
import android.view.View

class DisplaySnackbar {

    constructor(view: View, message: String) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
            .setAction("Action", null).show()
    }
}